import tweepy
import couchdb
import json
import math
import sys
from datetime import datetime
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from geopy import geocoders
import logging
import datetime

adress=sys.argv[1]#"Melbourne"
halfradius=float(sys.argv[2])#100 in Km

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(adress+'.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


couch = couchdb.Server()
couch = couchdb.Server('http://115.146.94.154:5984/') #ipaddress
#db = couch.create(adress) # newly created
db = couch[adress] # existing database

consumer_key="WFQziQaYcVEFIxpYt6G6KZpT5"
consumer_secret="mLGfQuMky9PwwuEvit0ON1TXVziXSrpcA58DrfbCrwzIbJ4BEt"
access_token="713291891492564992-vJZytipG3Utkejn3WrpUDxZzyrYf0w5"
access_secret="2HK0UxUFaJdrSrqN4eLAJDOodjbub7pVXlx0pJSPc56FA"

# degrees to radians
def deg2rad(degrees):
    return math.pi*degrees/180.0
# radians to degrees
def rad2deg(radians):
    return 180.0*radians/math.pi

# Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

# Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

# Bounding box surrounding the point at given coordinates,
# assuming local approximation of Earth surface as a sphere
# of radius given by WGS84
def boundingBox(latitudeInDegrees, longitudeInDegrees, halfSideInKm):
    lat = deg2rad(latitudeInDegrees)
    lon = deg2rad(longitudeInDegrees)
    halfSide = 1000*halfSideInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)
    # Latitude distance 
    latDelta = abs(halfSide/radius)
    # Longitude distance 
    lonDelta = abs(halfSide/pradius)

    latMin = lat - latDelta
    latMax = lat + latDelta
    lonMin = lon - lonDelta
    lonMax = lon + lonDelta

    return (rad2deg(latMin), rad2deg(lonMin), rad2deg(latMax), rad2deg(lonMax))

class Listener(StreamListener):

    def on_status(self, status):

        try:
            tweet=status._json
            tweet["_id"]=tweet["id_str"]
            del tweet["id_str"]
            #print(tweet["_id"])
            db.save(tweet)

        except Exception as e:
            #print("Status error: %s" % e)
            logger.info("Status error: "+ e)

    def on_error(self, status_code):
        logger.info("Listener error: "+ status_code)
        #print("Listener error: %s" % status_code)
        #return True # Don't kill the stream

    def on_timeout(self):
        logger.info("Time out")
        #print("Time out: %s" % status_code)
        #return True # Don't kill the stream


#adress=sys.argv[1]#"Melbourne"
#halfradius=float(sys.argv[2])#100 in Km

g = geocoders.GoogleV3()
place, (lat, lng) = g.geocode(adress)
border=boundingBox(lng,lat,halfradius)
#print(border)
location = [border[0],border[1],border[2],border[3]]
#print ("Location of %s: %s, %s"% (place,lng,lat))

while True:
    try:
        logger.info("Stream error: "+ datetime.datetime.now().time().isoformat())
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_secret)
        twitterStream = Stream(auth, Listener())

        #twitterStream.filter(track=['melbourne']), async=True)
        twitterStream.filter(locations=location)#, async=True)

    except Exception as e:
        logger.info("Stream error: "+ e)
        #print("Stream error: %s" % e)
