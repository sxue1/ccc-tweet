import boto 
from boto.ec2.regioninfo import RegionInfo  

reg_name='melbourne'
reg_ep='nova.rc.nectar.org.au'
access='e82fff8302ef4a4a91a6fcaec0598375'
secret='53fb99e0b4cb4957a2b4be2ae3f5e7de'

region=RegionInfo(name=reg_name, endpoint=reg_ep)  
ec2_conn = boto.connect_ec2(aws_access_key_id=access, 
                            aws_secret_access_key=secret, 
                            is_secure=True, region=region, port=8773, 
                            path='/services/Cloud', 
                            validate_certs=False) 


reservation=ec2_conn.run_instances('ami-00003801',#NeCTAR Ubuntu 14.04 (Trusty) amd64
                                   key_name='COMP90024',
                                   instance_type='m1.medium',
                                   placement='melbourne-np',
                                   security_groups=['couchdb',
                                                    'default',
                                                    'http/s',
                                                    'ssh'])

vol = ec2_conn.create_volume(50, "melbourne-np")
ec2_conn.attach_volume (vol.id, instance.id, "/dev/vdc")
