# Team: 4; Melbourne; Johanes G Siregar(685003)

#!/bin/bash

# Update OS
sudo do-release-upgrade
sudo apt-get update && sudo apt-get dist-upgrade -y
sudo apt-get install software-properties-common -y

# Format disk
sudo mkfs.ext4 /dev/vdc

# Label disk
sudo e2label /dev/vdc "volume1"

# Create mount directory
sudo mkdir /vol

# Mount disk
sudo mount /dev/vdc /vol
# Add auto mount config
sudo cat /etc/fstab > fstab.bak
sudo echo "/dev/vdc        /vol    auto    defaults,nofail,comment=cloudconfig     0       2">>fstab.bak
sudo cp -rf fstab.bak /etc/fstab

# Install couchdb
## Add PPA for stable couchdb and update package
sudo add-apt-repository ppa:couchdb/stable -y
sudo apt-get update
## Install
sudo build-dep install couchdb -y
sudo apt-get install couchdb -y
## Test
curl localhost:5984

# Secure couchdb
## Stop server
sudo service couchdb stop
## Create couchdb folder on volume
sudo mkdir /vol/couchdb
## Change ownership
sudo chown -R couchdb:couchdb /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Change permission
sudo chmod -R 0770 /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Backup config
sudo cat /etc/couchdb/local.ini>couchdb.bak
## Update config Note: copy couchdb.ini file first
head -9 couchdb.bak > local.ini
echo 'database_dir = /vol/couchdb' >> local.ini
echo 'view_index_dir = /vol/couchdb' >> local.ini
echo '' >> local.ini
echo '[httpd]' >> local.ini
echo 'port = 5984' >> local.ini
echo 'bind_address = 0.0.0.0' >> local.ini
sudo cp -rf local.ini /etc/couchdb/local.ini

## Start server
sudo service couchdb start
