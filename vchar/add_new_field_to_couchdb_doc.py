from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import couchdb, json, re, ngram

class UpdateDB():
    # add_field     : field to be added to the couchdb document with field_value
    # field_value   : value or marker of the field represent the type of tweet, i.e.: alcohol:y
    # threshold     : ngram threshold
    def __init__(self, source_add, source_port, source_db, filename, add_field, field_value, threshold):
        self.source_add = source_add
        self.source_port = source_port
        self.source_db = source_db
        self.filename = filename
        self.add_field = add_field
        self.field_value = field_value
        self.threshold = threshold


    def update(self):

        src_couch = couchdb.Server('http://'+self.source_add+':'+self.source_port)
        src_db = src_couch[self.source_db]


        f = open('update_'+self.source_db, 'a')
        terms = self.get_list(self.filename)
        print len(terms)
        print terms

        print "total # documents to be updated ", len(src_db)

        for doc in src_db:
            record = src_db[doc]
            record[self.add_field]=self.field_value
            try: 
                src_db.save(record)
                
            except couchdb.http.ResourceConflict:
                f.write("unable to update record "+record['_id']+"\n")
                print "unable to update record"

    def lcase(self, text):
        return text.lower()


    def get_list(self, filename):
        terms = []
        with open(filename, 'r') as input:
            for term in input:
                if term[0] != '#' and term not in terms:
                    terms.append(self.lcase(term.replace('\n','')))
        return terms


    def sentiment(self, text, terms, threshold):
        words = self.lcase(text).split(' ')
        terms 
        # print ("length words %d" % len(words))
        for word in words:
            for term in terms:
                if ngram.NGram.compare(word, term) > threshold:
                    return True
        return False


if __name__ == '__main__':
    udb = UpdateDB('115.146.94.xxx','5984','nsw', 'alcohol_terms.txt', 'alcohol', 'y', 0.5) 
    udb.update()   
