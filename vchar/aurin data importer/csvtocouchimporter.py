import couchdb, re, uuid

# this importer will generate uniq id for each new record adn all data 
# of the csv file will be imported.
# server 	: ip address or name of server
# port		: port of the couchdb server
# dbname	: database that data is going to import to
# csvfile	: csv raw data file
class CSVImporter():
	def __init__(self, server, port, dbname, csvfile):
		self.server = server
		self.port = port
		self.dbname = dbname
		self.csvfile = csvfile

	def importdata(self):
		couch = couchdb.Server('http://'+self.server+':'+self.port+'/')
		db = couch[self.dbname]

		title = []
		row = dict()
		with open(self.csvfile, 'r') as input:
			for i, line in enumerate(input):
				line = line.replace("\n","")
				if i != 0:
					line = line.split(",")
					row['_id'] = str(uuid.uuid1()) 
					for j, c in enumerate(line):
						row[title[j]] = line[j].replace('"','').strip()
					
					db.save(row)
				else:
					title = [t.strip() for t in line.split(",")]
			
#115.146.89.81:5984/
if __name__ == '__main__':
	print "start importing"
	csvimport = CSVImporter('115.146.89.81','5984','aurin1','data327152835682013751.csv')
	csvimport.importdata()
	print "finish importing"