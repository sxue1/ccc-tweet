from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import couchdb, json, re

class MigrateDB():
    def __init__(self, source_add, source_port, source_db, dest_add, dest_port, dest_db, city, state, year):
        self.source_add = source_add
        self.source_port = source_port
        self.source_db = source_db
        self.dest_add = dest_add
        self.dest_port = dest_port
        self.dest_db = dest_db
        self.city = city
        self.state = state
        self.year = year

    def migrate(self):

        src_couch = couchdb.Server('http://'+self.source_add+':'+self.source_port)
        src_db = src_couch[self.source_db]

        #couch_dest = couchdb.Server('http://115.146.89.81:5984/')
        des_couch = couchdb.Server('http://'+self.dest_add+':'+self.dest_port)
        des_db = des_couch[self.dest_db]

        f = open('src'+self.source_db, 'a')
        fd = open('dest'+self.dest_db, 'a')

        print "total # documents to be migrated ", len(src_db)

        for doc in src_db:
            record = src_db[doc]
            del record['_rev']
            if 'id_str' in record:
                del record['_id']
                record['_id'] = record['id_str']
                del record['id_str']
            try: 
                if not record['place']:
                    print record['_id'] + 'is not migrated'
                else:
                    place = record['place']['full_name']
                    if  self.city in place or self.state in place:
                        if record['created_at'].split(" ")[-1] >= self.year:
                            print record['_id'] + place + " is in " + self.city + " or " + self.state
                            des_db.save(record)
                        else:
                            pass
                    else:
                        print record['_id'] + place + " is not in " + self.city + " or " + self.state
                        fd.write(record['_id']+"\n")
                del src_db[doc]
            except couchdb.http.ResourceConflict:
                f.write(record['_id']+"\n")
                print "conflict"
                del src_db[doc]

if __name__ == '__main__':
    mig = MigrateDB('localhost','5984','victoria','localhost','5984','melbourne','Melbourne', 'Victoria', 2013) 
    mig.migrate()   
