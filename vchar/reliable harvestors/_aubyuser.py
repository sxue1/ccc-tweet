from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import couchdb, json, tweepy, time

class listener(StreamListener):
    def __init__(self, db, location_name, year):
        self.location_name = location_name
        self.year = year
        self.db = db

    def tweetsbyuser(self, userid, location_name, year):
        api = tweepy.API(auth)
        tweets = api.user_timeline(screen_name = userid, count = 1200, include_rts = True)
        
        for tweet in tweets:
            tweet = json.loads(json.dumps(tweet._json))
            tweet['_id'] = tweet['id_str']
            del tweet['id_str']
            try:
                if tweet['place'] is not None and location_name in tweet['place']['country']:
                    if tweet['created_at'].split(" ")[-1] >= year:
                        print tweet['place']['full_name']
                        self.db.save(tweet)
            except couchdb.http.ResourceConflict:
                pass

    def on_data(self, data):
        if data[0].isdigit():
            pass
        else:
            jline = json.loads(data)
            jline[u'_id'] = jline['id_str']
            del jline['id_str']
            self.tweetsbyuser(jline['user']['screen_name'], self.location_name, self.year)

    def on_error(self, status):
        print status

class TweetsImporter():
    def __init__(self, auth, db, coordonates, location_name, year):
        self.db = db
        self.coordonates = coordonates
        self.location_name = location_name
        self.year = year
        self.auth = auth

    def stream_tweets(self):
        while True:
            try:
                twitterStream = Stream(self.auth, listener(self.db, self.location_name, self.year))
                twitterStream.filter(locations=self.coordonates)
            except tweepy.TweepError:
                time.sleep(60 * 5)  # retry after 5 minutes
                continue
            except StopIteration:
                break

if __name__ == '__main__':
    ckey = 'kJVxx68APKCcEHOBmxUjizxtc'
    csecret = '8ofEsJAjinQMpBmeSmnwFvsFQ5iSg0jd98eAaohIafwqUVXKfq'
    atoken = '724023976582737921-0hFrMawqDgfTZNLOf8iQUx30Wedw5JI'
    asecret = 'mgFz2VctIJ2jb8xYvCgkno01Eb2YQn2PoOxVy8wHZKinM'

    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    couch = couchdb.Server('http://localhost:5984/')
    db = couch['au']
    
    australia = [112.921114,-43.740482,159.109219,-9.142176]
    importer = TweetsImporter(auth,db,australia,"Australia", 2013)
    importer.stream_tweets()
