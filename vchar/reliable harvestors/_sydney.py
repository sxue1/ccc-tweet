from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import couchdb, json, tweepy, time

class listener(StreamListener):
    def __init__(self, db, location_name, year):
        self.location_name = location_name
        self.year = year
        self.db = db

    def tweetsbyuser(self, userid, location_name, year):
        api = tweepy.API(auth)
        tweets = api.user_timeline(screen_name = userid, count = 1200, include_rts = True)
        
        for tweet in tweets:
            tweet = json.loads(json.dumps(tweet._json))
            tweet['_id'] = tweet['id_str']
            del tweet['id_str']
            try:
                if tweet['place'] is not None and location_name in tweet['place']['country']:
                    if tweet['created_at'].split(" ")[-1] >= year:
                        print tweet['place']['full_name'] + tweet['created_at'].split(" ")[-1]
                        self.db.save(tweet)
            except couchdb.http.ResourceConflict:
                pass

    def on_data(self, data):
        if data[0].isdigit():
            pass
        else:
            jline = json.loads(data)
            jline[u'_id'] = jline['id_str']
            del jline['id_str']
            self.tweetsbyuser(jline['user']['screen_name'], self.location_name, self.year)

    def on_error(self, status):
        print status

class TweetsImporter():
    def __init__(self, auth, db, coordonates, location_name, year):
        self.db = db
        self.coordonates = coordonates
        self.location_name = location_name
        self.year = year
        self.auth = auth

    def stream_tweets(self):
        while True:
            try:
                twitterStream = Stream(self.auth, listener(self.db, self.location_name, self.year))
                twitterStream.filter(locations=self.coordonates)
            except tweepy.TweepError:
                time.sleep(60 * 5)  # retry after 5 minutes
                continue
            except StopIteration:
                break

if __name__ == '__main__':
    ckey = 'jYZJ71BnFqQUibUtkcAqBpJ8d'
    csecret = '9FmqAtjoQcFR9or40ItKhkS9j0UsgZrjuQDmPb4ZCkNTXZqTVp'
    atoken = '497249797-bz2QZ9oXtoAah5z0SKy7t2aB2iwakaiJBKgIwoDl'
    asecret = 'imCXUmhuS2RXrm5aJT6Vit2JgYadtsZWLcrPd0m2MPT0f'

    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    couch = couchdb.Server('http://localhost:5984/')
    db = couch['nsw']
    
    sydney = [150.30798016646708, -34.89332610342476, 152.10600023353297, -32.84164769657525]
    importer = TweetsImporter(auth,db,sydney,"Australia", 2013)
    importer.stream_tweets()
