from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import couchdb, json, tweepy, time

class listener(StreamListener):
    def __init__(self, db, country, year):
        self.db = db
        self.country = country
        self.year = year

    def tweetsbyuser(self, userid, country, year):
        api = tweepy.API(auth)
        tweets = api.user_timeline(screen_name = userid, count = 1200, include_rts = True)
        
        for tweet in tweets:
            tweet = json.loads(json.dumps(tweet._json))
            tweet['_id'] = tweet['id_str']
            del tweet['id_str']
            try:
                if tweet['place'] is not None and tweet['created_at'].split(" ")[-1] >= self.year:
                    place = tweet['place']['country']
                    if self.country in place:
                        print tweet['place']['full_name'] + tweet['created_at'].split(" ")[-1]
                        self.db.save(tweet)
                    else:
                        pass
                else:
                    if tweet['place'] is not None:
                        print "old data from " + tweet['place'] + " - " + tweet['created_at'].split(" ")[-1]
            except couchdb.http.ResourceConflict:
                pass

    def on_data(self, data):
        if data[0].isdigit():
            pass
        else:
            jline = json.loads(data)
            print jline['user']['screen_name']
            self.tweetsbyuser(jline['user']['screen_name'], self.country, self.year)

    def on_error(self, status):
        print status

class TweetsImporter():
    def __init__(self, auth, db, coordonates, country, year):
        self.db = db
        self.coordonates = coordonates
        self.country = country
        self.auth = auth
        self.year = year

    def stream_tweets(self):
        while True:
            try:
                twitterStream = Stream(self.auth, listener(self.db, self.country, self.year))
                twitterStream.filter(locations=self.coordonates)
            except tweepy.TweepError:
                time.sleep(60 * 5)  # retry after 5 minutes
                continue
            except StopIteration:
                break

if __name__ == '__main__':
    ckey = 'VOq9Lgx7iRu16v93i8TWjASWb'
    csecret = 't780m3ZLIlQVMtsiom0PL7CmaqH27ZZTc3Lu2PEyIWOn8JO3N5'
    atoken = '1130198484-qaXOydMZVMwPKNFluQVNzBr6zAM4P8fQmBh0Luo'
    asecret = 'PvX3u8VkZrlkaK1vPvXjVUIQjA7fljaDggQEN3kVIp6gc'

    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    couch = couchdb.Server('http://localhost:5984/')
    db = couch['victoria']
    
    melbourne = [144.0639765158716, -38.91244694926847, 145.86258348412844, -36.71576705073153]
    importer = TweetsImporter(auth,db,melbourne, "Australia", 2012)
    importer.stream_tweets()
