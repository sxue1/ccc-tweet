# Team: 4; Melbourne; Johanes G. Siregar (685003)

# Super User Mode
# sudo su
# Update OS
sudo do-release-upgrade
sudo apt-get update && sudo apt-get dist-upgrade
sudo apt-get install software-properties-common -y
# List disk
sudo lsblk -f
sudo fdisk -l
# Check partitions
sudo parted /dev/vda1 'print'
sudo parted /dev/vdb 'print'
sudo parted /dev/vdc 'print'
#parted /dev/vdc mklabel gpt
#parted /dev/vdc mkpart primary ext4 0G 100G
# Format disk
sudo mkfs.ext4 /dev/vdc
# Label disk
sudo e2label /dev/vdc "volume1"
# Create mount directory
sudo mkdir /vol
# Mount disk
sudo mount /dev/vdc /vol
# Add auto mount config
echo "/dev/vdc        /vol    auto    defaults,nofail,comment=cloudconfig     0       2">>/etc/fstab
# Install couchdb
## Add PPA for stable couchdb and update package
sudo add-apt-repository ppa:couchdb/stable -y
sudo apt-get update
## Install
sudo apt-get install couchdb -y
## Test
curl localhost:5984
# Secure couchdb
## Stop server
sudo service couchdb stop
## Create couchdb folder on volume
sudo mkdir /vol/couchdb
## Change ownership
sudo chown -R couchdb:couchdb /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Change permission
sudo chmod -R 0770 /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Backup config
sudo cat /etc/couchdb/local.ini>couchdb.bak
## Update config Note: copy couchdb.ini file first
sudo head -9 couchdb.bak > /etc/couchdb/local.ini
sudo tail -45 couchdb.ini >> /etc/couchdb/local.ini
## Start server
sudo service couchdb start
sudo curl -X PUT http://localhost:5984/melbourne/_security -u "admin:adminccc024" -d '{"admins":{"names":["admin"], "roles":[]}}'
sudo curl -HContent-Type:application/json \
-vXPUT $HOST/_users/org.couchdb.user:dbadmin \
--data-binary '{"_id": "org.couchdb.user:dbadmin","name": "dbadmin","roles": [],"type": "admin","password": "dbpass024"}'
curl -vX PUT $HOST/melbourne/_security  \
-Hcontent-type:application/json \
--data-binary '{"admins":{"names":["admin_db"],"roles":[]},"members":{"names":["dbadmin"],"roles":[]}}'
