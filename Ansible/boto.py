# Team: 4; Melbourne; Johanes G. Siregar (685003)
# 
"""boto.py: Handles instance & volume creation, 
            then attaching volume to instance and
            output instance ip address to host file  

__author__ = "Johanes G. Siregar"
__copyright__ = "Copyright 2016, COMP90024 Project2"

__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "siregarj@student.unimelb.edu.au"
__status__ = "Production"

Example:

    $ python3 boto.py hostfile(str) hostno(int)

    e.g. $ python3 boto.py hosts 4


Note:

	* Login to nectar dashboard
	* Compute > Access & Security > API Access
	* Download EC2 Credentials
	* Unpack the download zip file
	* Open 'ec2rc.sh file
	* Copy access string from EC2_ACCESS_KEY='the_access_code_str'
	* Copy secret string from EC2_SECRET_KEY='the_secret_code_str'
"""

# Required packages
import boto, time
from boto.ec2.regioninfo import RegionInfo 


# 'createsecuritygroup' function
 'createinstance' functiontesecuritygroup(ec2_conn):
    sg = ec2_conn.create_security_group('COMP90024', 'Project 2')
    sg.authorize('tcp', 5984, 5984, '0.0.0.0/0')
    sg.authorize('tcp', 3000, 3000, '0.0.0.0/0')
    sg.authorize('tcp', 22, 22, '0.0.0.0/0')
    sg.authorize('icmp', -1, -1, '0.0.0.0/0')
    return ec2_conn.get_all_security_groups()


# 'waitinstance' function
def waitinstance(instance):
    # Q: http://stackoverflow.com/questions/8070186/boto-ec2-create-an-instance-with-tags
    # A: http://stackoverflow.com/users/1345461/craig-ferguson
    while instance.update() == 'pending':
        time.sleep(3)

    # Now that the status is running, it's not yet launched. The only way to tell if it's fully up is to try to SSH in.
    if instance.update() == "running":
        retry = True
        while retry:
            try:
                print('ready for SSH connection')# SSH into the box here. I personally use fabric
                retry = False
            except:
                print("%s status\t: %s"%(instance.id,instance.status))
                time.sleep(1)


# 'createinstance' function
def createinstance(ec2_conn,sg):
    reservation=ec2_conn.run_instances('ami-00003801',#NeCTAR Ubuntu 14.04 (Trusty) amd64 \
                                       key_name='COMP90024',
                                       instance_type='m1.medium',
                                       placement='melbourne-qh2',
                                       security_groups=sg)

    instance = reservation.instances[0]
    waitinstance(instance)
    
    return instance.id, instance.ip_address


# 'createvolume' function
def createvolume(ec2_conn):
    volume = ec2_conn.create_volume(125, "melbourne-qh2")
    while volume.status != 'available':
        print ("%s status\t: %s"%(volume.id,volume.status))
        time.sleep(3)
        volume.update()
        
    return volume.id


# 'writefile' function
def writefile(filename,text):
    with open(filename, "a") as myfile:
        myfile.write("%s\n" % text)


# 'writehostfile' function
def writehostfile(hostfile, nodeno, ip):
    if nodeno < 3:
        writefile(hostfile, "[harvestor]") 
    elif nodeno == 3:
        writefile(hostfile, "[analyser]")
    else:
        writefile(hostfile, "[server]")
        
    writefile(hostfile, ip) 
    writefile(hostfile, "\n") 


# 'execution' function
def execution(ec2_conn, args, sg):
    for i in range(args[1]):
        no = i+1
        print("Creating instance %s" % no)
        instanceid, instanceip = createinstance(ec2_conn, sg)

        print("Creating volume %s" % no)
        volumeid = createvolume(ec2_conn)
   
        print("Attaching volume %s to instance %s" % (no,no))
        ec2_conn.attach_volume (volumeid, instanceid, "/dev/vdc")
        writehostfile(args[0],args[1])


# 'main' function
def main(args):
    reg_name='melbourne'
    reg_ep='nova.rc.nectar.org.au'
    EC2_ACCESS_KEY='...put access key here...'
    EC2_SECRET_KEY='...put secret key here...'

    region=RegionInfo(name=reg_name, endpoint=reg_ep)  
    connection = boto.connect_ec2(aws_access_key_id=EC2_ACCESS_KEY, \
                                  aws_secret_access_key=EC2_SECRET_KEY,\
                                  is_secure=True, region=region, port=8773, \
                                  path='/services/Cloud', validate_certs=False) 
    securitygroup = createsecuritygroup(connection)
    execution(connection, args, securitygroup)   


if __name__ == "__main__":
    main(sys.argv[1:])
