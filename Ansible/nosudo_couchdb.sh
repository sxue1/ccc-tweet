#!/bin/bash

# Update OS
do-release-upgrade
apt-get update && apt-get dist-upgrade -y
apt-get install software-properties-common -y

# Format disk
mkfs.ext4 /dev/vdc

# Label disk
e2label /dev/vdc "volume1"

# Create mount directory
mkdir /vol

# Mount disk
mount /dev/vdc /vol
# Add auto mount config
cat /etc/fstab > fstab.bak
echo "/dev/vdc        /vol    auto    defaults,nofail,comment=cloudconfig     0       2">>fstab.bak
cp -rf fstab.bak /etc/fstab

# Install couchdb
## Add PPA for stable couchdb and update package
add-apt-repository ppa:couchdb/stable -y
apt-get update
## Install
build-dep install couchdb -y
apt-get install couchdb -y
## Test
curl localhost:5984

# Secure couchdb
## Stop server
service couchdb stop
## Create couchdb folder on volume
mkdir /vol/couchdb
## Change ownership
chown -R couchdb:couchdb /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Change permission
chmod -R 0770 /usr/lib/couchdb /usr/share/couchdb /etc/couchdb /usr/bin/couchdb /vol/couchdb
## Backup config
cat /etc/couchdb/local.ini>couchdb.bak
## Update config Note: copy couchdb.ini file first
head -9 couchdb.bak > local.ini
echo 'database_dir = /vol/couchdb' >> local.ini
echo 'view_index_dir = /vol/couchdb' >> local.ini
echo '' >> local.ini
echo '[httpd]' >> local.ini
echo 'port = 5984' >> local.ini
echo 'bind_address = 0.0.0.0' >> local.ini
cp -rf local.ini /etc/couchdb/local.ini

## Start server
service couchdb start
