# Team 4; Melbourne; Johanes G. Siregar(685003)

#!/usr/bin/env python3
"""harvest_city.py: Utilises Twitter's stream API to harvest tweets
                    which filtered with a bounding box with lower-left
                    corner and upper-right corner lat, long pairs measured
                    from center coordinate of a city upto a certain radius
                    in Km then store the tweet to couchdb using tweet ids
                    as key and also logs all error to a file

__author__ = "Johanes G. Siregar"
__copyright__ = "Copyright 2016, COMP90024 Project2"

__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "siregarj@student.unimelb.edu.au"
__status__ = "Production"

Example:

    $ python3 harvest_city.py city_name(str) radius_in_km(int)

    e.g. $ python3 harvest_city.py melbourne 100

Note:
    * Twitter's API key & secret must be provided in the script
    * Couchdb server path to save DB must also included as well
"""

# Required packages
import requests, json 
import tweepy
import couchdb
import json
import math
import sys
from datetime import datetime
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from geopy import geocoders
import logging
import datetime

# Get arguments
adress=sys.argv[1]
halfradius=float(sys.argv[2])

# Error logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(adress+'.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# Create couchdb
couch = couchdb.Server()
couch = couchdb.Server('http://115.146.94.154:5984/')
db = couch.create(adress) 
db = couch[adress] 

# Twitter API params
consumer_key="WFQziQaYcVEFIxpYt6G6KZpT5"
consumer_secret="mLGfQuMky9PwwuEvit0ON1TXVziXSrpcA58DrfbCrwzIbJ4BEt"
access_token="713291891492564992-vJZytipG3Utkejn3WrpUDxZzyrYf0w5"
access_secret="2HK0UxUFaJdrSrqN4eLAJDOodjbub7pVXlx0pJSPc56FA"

# Calculate bounding box from radius
## degrees to radians
def deg2rad(degrees):
    return math.pi*degrees/180.0
## radians to degrees
def rad2deg(radians):
    return 180.0*radians/math.pi

## Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

## Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

## Bounding box surrounding the point at given coordinates,
## assuming local approximation of Earth surface as a sphere
## of radius given by WGS84
def boundingBox(latitudeInDegrees, longitudeInDegrees, halfSideInKm):
    lat = deg2rad(latitudeInDegrees)
    lon = deg2rad(longitudeInDegrees)
    halfSide = 1000*halfSideInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)
    # Latitude distance 
    latDelta = abs(halfSide/radius)
    # Longitude distance 
    lonDelta = abs(halfSide/pradius)

    latMin = lat - latDelta
    latMax = lat + latDelta
    lonMin = lon - lonDelta
    lonMax = lon + lonDelta

    return (rad2deg(latMin), rad2deg(lonMin), rad2deg(latMax), rad2deg(lonMax))

# Listener class for Twitter API listener
class Listener(StreamListener):

    def on_status(self, status):

        try:
            tweet=status._json
            tweet["_id"]=tweet["id_str"]
            del tweet["id_str"]
            db.save(tweet)

        except Exception as e:
            logger.info("Status error: "+ e)

    def on_error(self, status_code):
        logger.info("Listener error: "+ status_code)

    def on_timeout(self):
        logger.info("Time out")

## Bounding box parameters
g = geocoders.GoogleV3()
place, (lat, lng) = g.geocode(adress)
border=boundingBox(lng,lat,halfradius)
location = [border[0],border[1],border[2],border[3]]

# Keep the stream alive
while True:
    try:
        logger.info("Stream error: "+ datetime.datetime.now().time().isoformat())
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_secret)
        twitterStream = Stream(auth, Listener())

        twitterStream.filter(locations=location)

    except Exception as e:
        logger.info("Stream error: "+ e)
