# Team: 4; Melbourne; Rahul Sharma (706453)

from textblob import TextBlob
import couchdb
couch = couchdb.Server('http://115.146.89.74:5984')
couch.resource.credentials = ('admin_db','pass024db')
try:
    couch.create('sentiment_results')
except:
    print('DB exists. Resetting now...')
    couch.delete('sentiment_results')
    couch.create('sentiment_results')
    print('DB reset.')

db = couch['sentiment_results']
if(couch['melbourne'] is not None):
    print('Twitter DB status OK.')
    tw_db = couch['melbourne']
    counter = 0
    for tweet in tw_db:
        counter = counter + 1
        content = ''
        try:
            content = (tw_db[tweet]['text'])
        except KeyError:
            print('Cannot find content.')
# try
        place = ''
        try:
            place = (tw_db[tweet]['place']['name'])
# catch
        except TypeError:
            try:
                place = (tw_db[tweet]['place']['full_name'])
            except TypeError:
                print('No place name presents. Ignore...')
                place = 'unknown'
        blob = TextBlob(content)
        polarity = blob.sentiment.polarity
        subjectivity = blob.sentiment.subjectivity
        doc = {'suburb_name': place, 'polarity': polarity, 'subjectivity': subjectivity}
        try:
            db.save(doc)
        except:
            print('Saving to db failed')

        print('Sentiment data saved. No.' + str(counter))
