# Team: 4; Melbourne; Song Xue (667692)

import couchdb
import json

couch = couchdb.Server('http://115.146.89.74:5984')
couch.resource.credentials = ('admin_db','pass024db')
try:
    couch.create('vic_income')
except:
    print('DB exists. Resetting now...')
    couch.delete('vic_income')
    couch.create('vic_income')
    print('DB reset')

db = couch['vic_income']
try:
    with open("income.json") as json_file:
        json_data = json.load(json_file)
        features = json_data['features']
        for f in features:
            try:
                name = f['properties']['SSC_NAME']
                income = f['properties']['median11']
                doc = {'surburb_name' : name , 'income' : income}
                db.save(doc)
                print('Doc saved')
            except:
                print('Cannot find properties. Ignore.')
except:
    print('Cannot file income.json file.')
