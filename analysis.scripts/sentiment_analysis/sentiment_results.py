# Team: 4; Melbourne; Rahul Sharma (706453)

import couchdb
import json
import urllib.request as ur
counter = 0
couch = couchdb.Server('http://115.146.89.74:5984')
couch.resource.credentials = ('admin_db','pass024db')
try:
    couch.create('sentiment_correlation')
except:
    print('DB exists. Resetting now...')
    couch.delete('sentiment_correlation')
    couch.create('sentiment_correlation')
    print('DB reset')
income = {}
try:
    income_db = couch['vic_income']
except:
    print('Cannot find vic_income db')

print('Building income dictionary')
for i in income_db:
    income_num = 0
    name = ''
    try:
        income_num = income_db[i]['income']
        name = income_db[i]['surburb_name']
        print('Status Ok.')
    except TypeError:
        print('Value is null, throw TypeError exception.')
    income[name] = income_num
db = couch['sentiment_correlation']
if(couch['sentiment_results'] is not None):
    print('Twitter Analysis Results exist. Status Ok.')
    # Get the view results
    json_result = ur.urlopen('http://115.146.89.74:5984/sentiment_results/_design/sentiment_avg/_view/sentiment_avg?reduce=true;group_level=1')
    json_string = json_result.read().decode("utf8")
    res = json.loads(json_string)
    for j in res['rows']:
        counter += 1
        place = j['key']
        val = j['value']
        doc = {}
        if income.get(place) is not None:
            doc = {'suburb_name': place, 'income': income.get(place), 'sentiment_result': val}
            db.save(doc)
        else:
            print('T: ' + place)
        print('Result saved. #' + str(counter))
