# Team: 4; Melbourne; Johanes G. Siregar (685003)

#!/usr/bin/env python3
"""view_parser.py: Reads couchdb view, print out the top counter  
                   of accumulated counter in decending order 
                   to a certain limit for each key in array key 

__author__ = "Johanes G. Siregar"
__copyright__ = "Copyright 2016, COMP90024 Project2"

__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "siregarj@student.unimelb.edu.au"
__status__ = "Production"

Example:

    $ python3 view_parser.py path(url) group_level(int) limit(int)

"""

import json, requests, sys, pprint

def assignvalue(key,dic,val):
    """assignvalue: check if key value pair exist in a dictionary
                    if it not then assign the key value pair in dictionary 
                    otherwise accumulated current value with the new value
        Args:
            key (str) : a  key of string type 
            dic (dict) : a dictionary with string as key and integer as value
            val (int) : a value of integer type
        Returns:
            null
    """
    if( not key in dic):
        dic[key] = val
    else:
        dic[key] += val

def countbykey(jsonobj, grplvl):
    """countbykey: parses the json object to a list containing dictionaries
                   groupped for each key (from couchdb view's array list key)
                   with pairs of distinct key (in each group) and accumulate
                   the value as the content of each dictionary in a group
        Args:
            jsonobj (json) : a  key of string type 
            grplvl  (int) : a value of integer type
        Returns:
            result ([{}]) : a list of dictionary
    """
    temp = [{} for i in range(grplvl)]
    result = [{} for i in range(grplvl)]
         
    for row in jsonobj['rows']:
        for i in range(grplvl):
            assignvalue(row['key'][i],temp[i],row['value'])
        for i in range(grplvl):
            result[i] = sorted(temp[i].items(), key=lambda x: (x[1],x[0]), reverse=True)
    return result

def readview(path, grplvl = 0):
    """countbykey: reads couchdb view and return a json object
    
        Args:
            path (str) : a string representation of an url 
            grplvl (int) : integer value of the grouping level
        Returns:
            countbykey (func) : the return value of 'countbykey'
                                which is a list of dictionaries
    """
    if grplvl == 0:
        r = requests.get(path)
    else:
        payload = {'group_level': str(grplvl)}
        r = requests.get(path, \
                         params=payload)

    return countbykey(json.loads(r.text),grplvl)

def main(argv):
    """main: read all arguments and prints output of 'readview' function
    
        Args:
            path (str) : a string representation of an url 
            grplvl (int) : integer value of the grouping level
        Returns:
            countbykey (func) : the return value of 'countbykey'
                                which is a list of dictionaries
    """
    ## a url eg. 'http://115.146.89.74:5984/melbourne/_design/count/_view/pn_ht-at_dt_tm_tg-un'
    viewpath = argv[0]
    ## an integer value >= 0 eg. 0, 1, 2, ...
    grplvl = int(argv[1])
    ## an integer value >= 1 eg. 1, 2, 3, ...
    limit = int(argv[2])
    out = readview(viewpath, grplvl)
    pp = pprint.PrettyPrinter(indent=4)

    for i in range(grplvl):
        pp.pprint(out[i][0:limit])
    
    
if __name__ == "__main__":
    main(sys.argv[1:])
