# Team: 4; Melbourne; Johanes G. Siregar (685003)
# 
#!/usr/bin/env python3
"""view_parser.py: sort a view by using it's value as key

__author__ = "Johanes G. Siregar"
__copyright__ = "Copyright 2016, COMP90024 Project2"

__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "siregarj@student.unimelb.edu.au"
__status__ = "Production"

Example:

    $ python3 sort_view.py 

"""

# Required packages
import couchdb, requests, json


# Couchdb configuration
couch = couchdb.Server()
couch = couchdb.Server('http://115.146.89.149:5984/') #ipaddress
dbname = "tweet_by_usernames"
db = couch.create(dbname)
db = couch[dbname]


# Read a view
viewurl = 'http://115.146.89.149:5984/melbourne/_design/top10/_view/usernames'
payload = {'group_level': '1'}
r = requests.get(viewurl, \
                 params=payload)

json_r = json.loads(r.text)


# Create a new database with combine keys
for row in json_r['rows']:
    strval = str(row['value'])
    pname = row['key']
    
    if len(strval) == 1:
            count="0000"+strval
    if len(strval) == 2:
        count="000"+strval
    if len(strval) == 3:
        count="00"+strval
    if len(strval) == 4:
        count="0"+strval
            
    db.save({'_id' : count+'-'+pname, \
             'name' : pname, \
             'count' : row['value']})
