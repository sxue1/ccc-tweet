//# Team: 4; Melbourne; Johanes G. Siregar (685003)
<script type="text/javascript">
function(keys, values, rereduce) {
    var data = {und_sp: 0, en_sp: 0, ar_sp: 0, zh_sp: 0, jp_sp: 0, ko_sp: 0, es_sp: 0, th_sp: 0, tp_sp: 0};
    if (rereduce) {
        for (i = 0, l = values.length; i<l ; ++i) {
            data['und_sp'] += values[i]['und_sp']
            data['en_sp'] += values[i]['en_sp']
            data['ar_sp'] += values[i]['ar_sp']
            data['zh_sp'] += values[i]['zh_sp']
            data['jp_sp'] += values[i]['jp_sp']
            data['ko_sp'] += values[i]['ko_sp']
            data['es_sp'] += values[i]['es_sp']
            data['th_sp'] += values[i]['th_sp']
            data['tp_sp'] += values[i]['tp_sp']
        }
    } else {
        for (i = 0, l = values.length; i<l ; ++i) {
            data['und_sp'] += values[i]['und']
            data['en_sp'] += values[i]['en']
            data['ar_sp'] += values[i]['ar']
            data['zh_sp'] += values[i]['zh']
            data['jp_sp'] += values[i]['jp']
            data['ko_sp'] += values[i]['ko']
            data['es_sp'] += values[i]['es']
            data['th_sp'] += values[i]['th']
            data['tp_sp'] += values[i]['tp']
        }
    }

    return data
}
</script>
