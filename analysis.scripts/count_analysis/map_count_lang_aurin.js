//# Team: 4; Melbourne; Johanes G. Siregar (685003)
<script type="text/javascript">
function(doc) {
    var data = {}
    data['und'] = doc.Language_spoken_home_ns_P;
    data['en'] = doc.SEO_Persons;
    data['ar'] = doc.SOL_Arabic_P;
    data['zh'] = doc.SOL_Chin_lang_Tot_P;
    data['jp'] = doc.SOL_Japanese_P;
    data['ko'] = doc.SOL_Korean_P;
    data['es'] = doc.SOL_Spanish_P;
    data['th'] = doc.SOL_Thai_P;
    data['tp'] = doc.Total_P;
  
    sscname = doc.SSC_NAME.toLowerCase();
    
    emit(sscname, data);
}
</script>
