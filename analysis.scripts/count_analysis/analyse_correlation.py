# Team: 4; Melbourne; Johanes G. Siregar (685003)
#!/usr/bin/env python3
"""count_corr.py: Reads couchdb view, print out the top counter 
                  of accumulated counter in decending order
                  to a certain limit for each key in array key

__author__ = "Johanes G. Siregar"
__copyright__ = "Copyright 2016, COMP90024 Project2"

__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "siregarj@student.unimelb.edu.au"
__status__ = "Production"

Example:

    $ python3 analyse_correlation.py

"""
# Required packages
import requests, json 
import string
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# Read DBs
## Read employment data (Source: AURIN)
data ={}
emppath = 'http://115.146.89.149:5984/vic_emp_ssc_2011/_design/count/_view/employ'
empresult = requests.get(emppath)

empjson = json.loads(empresult.text)

for row in empjson['rows']:
    sscname = row['key']
    data[sscname] = {}
    sscname = row['key']
    stats = row['value']
    for prop in stats:
        data[sscname][prop] = stats[prop]

## Read population number (Source: AURIN)
langpath = 'http://115.146.89.74:5984/vic_lang_ssc_2011/_design/count/_view/lang'
grouping = {'group_level': '1'}
langresult = requests.get(langpath, \
                          params=grouping)

langjson = json.loads(langresult.text)

for row in langjson['rows']:
    sscname = row['key']
    if sscname in data.keys():
        data[sscname]['pop_c'] = row['value']['tp_sp']

## Read number of tweet group by place and combine with AURIN data
tweetpath = 'http://115.146.89.149:5984/melbourne/_design/count/_view/place_name'
payload = {'group_level': '2'}
r = requests.get(tweetpath, \
                 params=payload)

json_r = json.loads(r.text)

dataset = {}
maxvals = {}
sumvals = {}
values = ['twt', 'pop', 'inc']
datatypes = ['city', 'neighborhood']

for dtype in datatypes:
    dataset[dtype] = {}
    maxvals[dtype] = {}
    sumvals[dtype] = {}
    for avalue in values:
        maxvals[dtype][avalue]=0
        sumvals[dtype][avalue]=0

for row in json_r['rows']:
    placename = row['key'][0].lower()
    placetype = row['key'][1]
    valuecount = row['value']
    
    if (placename in data.keys()) and (placetype in datatypes):
        dataset[placetype][placename] = data[placename]
        dataset[placetype][placename]['twt_c'] = valuecount
        maxvals[placetype]['twt'] = max(maxvals[placetype]['twt'], \
                                        valuecount)
        maxvals[placetype]['pop'] = max(maxvals[placetype]['pop'], \
                                        data[placename]['pop_c'])
        maxvals[placetype]['inc'] = max(maxvals[placetype]['inc'], \
                                        data[placename]['med_inc'])
        sumvals[placetype]['twt'] += valuecount
        sumvals[placetype]['pop'] += data[placename]['pop_c']
        sumvals[placetype]['inc'] += data[placename]['med_inc']

# Normalize data
for ptype in dataset:
    for pname in dataset[ptype]:
        
        if 'twt_c' in dataset[ptype][pname]:
            dataset[ptype][pname]['twt'] = dataset[ptype][pname]['twt_c'] / maxvals[ptype]['twt']
            del dataset[ptype][pname]['twt_c']
        else: 
            dataset[ptype][pname]['twt'] = 0
        
        if 'pop_c' in dataset[ptype][pname]:
            dataset[ptype][pname]['pop'] = dataset[ptype][pname]['pop_c'] / maxvals[ptype]['pop']
            del dataset[ptype][pname]['pop_c']
        else: 
            dataset[ptype][pname]['pop'] = 0
        
        if 'med_inc' in dataset[ptype][pname]:
            dataset[ptype][pname]['inc'] = dataset[ptype][pname]['med_inc'] / maxvals[ptype]['inc']
            del dataset[ptype][pname]['med_inc']
        else: 
            dataset[ptype][pname]['inc'] = 0

# Draw charts
## Put data to DataFrame
df1 = pd.DataFrame(dataset['city'])
df2 = pd.DataFrame(dataset['neighborhood'])

## Transpose the DataFrame
d1=pd.DataFrame(df1.transpose())
d2=pd.DataFrame(df2.transpose())

## Draw charts for city type
### Draw heatmap
sns.set(style="white")

### Compute the correlation matrix
corr = d1.corr()

### Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

### Set up the matplotlib figure
f, ax = plt.subplots(figsize=(8, 8))

### Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

### Draw the heatmap with the mask and correct aspect ratio
sns_heat = sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.5,
            square=True, linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
ax.set_title('Number of tweet correlated to employment (City)')
plt.savefig("heatplot_city.png", format='png', dpi=300)

### Draw pairplot
sns_pair = sns.pairplot(d1, size = 2)
plt.savefig("pairplot_city.png", format='png', dpi=300)

## Draw charts for neighborhood type
### Draw heatmap
corr = d2.corr()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(8, 8))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns_heat = sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.5,
            square=True, linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
ax.set_title('Number of tweet correlated to employment (Neighborhood)')
plt.savefig("heatplot_nhood.png", format='png', dpi=300)

### Draw pairplot
sns_pair = sns.pairplot(d2, size = 2)
plt.savefig("pairplot_nhood.png", format='png', dpi=300)


