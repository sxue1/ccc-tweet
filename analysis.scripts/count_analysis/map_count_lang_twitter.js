//# Team: 4; Melbourne; Johanes G. Siregar (685003)
<script type="text/javascript">
function(doc) {
  if(doc.place.name!=null) {
    if( doc.lang == 'und' ||
        doc.lang == 'en' ||
        doc.lang == 'ar' ||
        doc.lang == 'zh' ||
        doc.lang == 'jp' ||
        doc.lang == 'ko' ||
        doc.lang == 'es' ||
        doc.lang == 'th' ) {
      emit([doc.place.name.toLowerCase(),doc.lang], 1);
    }
  } 
}
</script>
