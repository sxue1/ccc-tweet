//# Team: 4; Melbourne; Johanes G. Siregar (685003)
<script type="text/javascript">
function(doc) {
  if(doc.user.screen_name!=null) {
    var re = /scien\w{2,6}/gi
    var keywords= doc.text.match(re);
    var ts = new Date(doc.created_at)
    var yyyy = ts.getFullYear().toString();                                    
    var mm = (ts.getMonth()+1).toString();         
    var dd  = ts.getDate().toString();             
    var date = yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]);

    var hr = ts.getHours().toString();                                    
    var hour = hr[1]?hr:"0"+hr[0]

    for (var i = 0; i < keywords.length; i++) {
      var results = [date, hour, doc.place.name.toLowerCase()]
      results.unshift(keywords[i].toLowerCase())
      results.unshift(doc.user.screen_name)
 
      emit(results, doc.text);
    }
  } 
}
</script>
