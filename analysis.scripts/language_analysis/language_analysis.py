# Team: 4; Melbourne; Rahul Sharma (706453)
# 
# coding: utf-8

# # import packages

# In[18]:

import requests,json


# # Read the total tweet for a suburb from tweet data in couchdb

# In[21]:

#totalpath = 'http://115.146.93.37:5984/melbourne/_design/count/_view/lang'
#totalpath = 'http://115.146.93.37:5984/victoria/_design/lang_dest/_view/destination_for_lang'
totalpath = 'http://115.146.89.74:5984/melbourne/_design/count/_view/lang'
payload = {'group_level': '1'}
s = requests.get(totalpath,                  params=payload)

json_s = json.loads(s.text)
print(json_s)


# # Create a dictionary with suburb name as key and number of tweets as value

# In[22]:

langtotal = {}
print(json_s)
for row in json_s['rows']:
    #print(row['key'][0].lower())
    langtotal[row['key'][0].lower()] = row['value']
    


# # Read aurin data from couchdb

# In[23]:

aurinpath = 'http://115.146.89.149:5984/vic_lang_ssc_2011/_design/count/_view/lang'
payload = {'group_level': '1'}
t = requests.get(aurinpath,                  params=payload)

json_t = json.loads(t.text)


# # Create a dictionary from aurin data with suburb as key and language counters as the value

# In[24]:

langaurin = {}
maxtp=0

for row in json_t['rows']:
    #print(row['key'][0].lower())
    langaurin[row['key']] = row['value']
    maxtp=max(maxtp,row['value']['tp_sp'])


# # Read count of tweets with certain languages from couchdb view and create JSON object from the result

# In[25]:

#counterpath = 'http://115.146.93.37:5984/melbourne/_design/count/_view/lang'
counterpath = 'http://115.146.89.74:5984/melbourne/_design/count/_view/lang'
payload = {'group_level': '2'}
r = requests.get(counterpath,                  params=payload)

json_r = json.loads(r.text)


# # Iterate 'rows' value in JSON object and create a dictionary with suburb as key and a dictionary of lang code and occurance counter pairs as value 

# In[26]:

langdata = {}
maxtotal=0
dataset={}
lang_list=['en','ar','ko','zh','es','th','und']
    
for row in json_r['rows']:
    suburb = row['key'][0].lower()
    lang = row['key'][1]
    count = row['value']
    
    if not (suburb in (langdata)):
        langdata[suburb]={}
        
    langdata[suburb][lang] =  count/langtotal[suburb]
    langdata[suburb]['total']=langtotal[suburb]
    maxtotal=max(maxtotal,langtotal[suburb])


# # populate missing langdata counter

# # Merge aurin twitter data

# In[27]:

dataset={}
lang_list=['en','ar','ko','zh','es','th','und']
exist = 0
found = []
notexist = 0
notfound = []
sumcount=0
temp={}
maxcount=0

for key,value in langdata.items():
    total = int(value['total'])
    if key in langaurin:
        
        #print(key)
        for langcode in lang_list:
            if not langcode in langdata[key]:
                langdata[key][langcode] = 0
                
            aurincode=langcode+"_sp"
            if  langaurin[key]['tp_sp'] == 0:
                value['tot_tp_r'] = total / maxtp
                value[aurincode+"_r"]=langaurin[key][aurincode] / maxtp
            else:
                value['tot_tp_r'] = total / langaurin[key]['tp_sp']
                value[aurincode+"_r"]=langaurin[key][aurincode] / langaurin[key]['tp_sp'] 
            if  total == 0:
                value[langcode+"_r"] = value[langcode] / maxtotal
            elif langcode in value:
                value[langcode+"_r"] = value[langcode] / total
            else:
                value[langcode+"_r"] = 0
            del value[langcode]
            #print (value)
        
        
        dataset[key] = value

        sumcount += total
        exist += 1
        found.append(key)
        maxcount=max(maxcount, total)
        del value['total']

    else:
        notexist += 1
        notfound.append(suburb)


# In[29]:

print(langtotal['clayton'])


# In[10]:

print(langaurin['clayton'])


# In[30]:

print(langdata['clayton'])


# In[12]:

#print(dataset['clayton'])
#for k,v in dataset.items():
 #   print(v['jp_sp_r'])


# In[31]:

#get_ipython().magic('matplotlib inline')

import string
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
#ion()

sns.set(style="white")
#rs = np.random.RandomState(33)
d = pd.DataFrame(dataset)
d_transpose = pd.DataFrame(d.transpose())
print(d_transpose)

corr = d_transpose.corr()
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

f, ax = plt.subplots(figsize=(11, 9))
cmap = sns.diverging_palette(220, 10, as_cmap=True)

#% matplotlib inline
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=1,
            square=True,linewidths=1, cbar_kws={"shrink": 1}, ax=ax)
plt.savefig("testfile.png",format='png',dpi=300)

sns.pairplot(d_transpose)




# In[ ]:



