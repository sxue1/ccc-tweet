#Team: 4; Melbourne; Song Xue (667692)

# Update apt-get
# sudo apt-get update
# Install apache server
#sudo apt-get install apache2
# Install Vim
#sudo apt-get install vim
# Install Tmux
#sudo apt-get install tmux
# Install Screen
#sudo apt-get install screen
# Install Nodejs
#sudo apt-get install nodejs
#sudo apt-get install nodejs-legacy 
# Install Npm
#sudo apt-get install npm
# Install Git
#sudo apt-get install git
# Clone the repo
#git clone https://bitbucket.org/sxue1/ccc-tweet
# Input username and password

# Install nginx server
#sudo -s
#nginx=stable
#add-apt-repository ppa:nginx/$nginx
#apt-get update
#apt-get install nginx
#exit

# NODE DEPLOY
#sudo npm install -g bower
# Install node dependency
#sudo npm install -g forever
#sudo npm install -g flightplan
#sudo npm install --save flightplan

cd /home/ubuntu/ccc-tweet/node.server
sudo npm install
# Run server with forever
forever start ./bin/www 

