//Team: 4; Melbourne; Song Xue (667692)

var express = require('express');
var router = express.Router();
var request = require('request');
var rp = require('request-promise');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Twitter Analysis Application' });
});

router.get('/scenario/income', function (req, res, next) {
    var sentiment = [];
    var income = [];
    var view_url = 'http://115.146.89.74:5984/sentiment_correlation/_design/sentiment/_view/sentiment_results?reduce=true;group_level=1';
    request(view_url, function(error, response, body){
        //Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_income = parsed_data['rows'][row]['key'];
                var s_sentiment = parsed_data['rows'][row]['value'];
                income.push(Math.round(s_income));
                sentiment.push(s_sentiment);
            }
            res.render('analysis1', {title: 'Sentiment analysis & Income', msg: JSON.stringify('Hello'), income_arr: JSON.stringify(income), sentiment_arr: JSON.stringify(sentiment)});
        } else {
            res.render('analysis1', {title: JSON.stringify(error)});
        }
    });
    });

router.get('/scenario/alcohol', function (req, res, next) {
    var sentiment = [];
    var alcohol = [];
    var bottleshop = [];
    var crime = [];
    var tweet = [];
    var traffic = [];
    var temp = [];
    var view_alcohol = 'http://115.146.89.81:5984/analysis05_vic201605102019/_design/analysis/_view/alcohol_vs_bottleshop?reduce=true;group_level=1';
    var view_crime = 'http://115.146.89.81:5984/analysis05_vic201605102019/_design/analysis/_view/crime_vs_bottleshop?reduce=true;group_level=1';
    var view_sentiment = 'http://115.146.89.81:5984/analysis05_vic201605102019/_design/analysis/_view/sentiment_vs_bottleshop?group_level=1;reduce=true';
    var view_traffic = 'http://115.146.89.81:5984/analysis05_vic201605102019/_design/analysis/_view/traffic_vs_bottleshop?reduce=true;group_level=1';
    var view_tweet = 'http://115.146.89.81:5984/analysis05_vic201605102019/_design/analysis/_view/tweet_vs_bottleshop?reduce=true;group_level=1';

    rp(view_alcohol)
		  .then(function(response){
		  	var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_bottleshop = parsed_data['rows'][row]['key'];
            var s_alcohol = parsed_data['rows'][row]['value'];
            bottleshop.push(s_bottleshop);
            alcohol.push(s_alcohol);
        }
		    return rp(view_crime)
		  })
		  .then(function(response){
        var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_crime = parsed_data['rows'][row]['value'];
            crime.push(s_crime);
        }
		    return rp(view_sentiment)
		  })
		  .then(function(response){
        var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_arr = (parsed_data['rows'][row]['value']);
            //console.log(s_arr[0]);
            var s_sentiment = s_arr[0];
            var s_tnum = s_arr[1];
            var s_avg = s_sentiment/s_tnum;
            sentiment.push(s_avg);
        }
        return rp(view_tweet)
		  })
		  .then(function(response){
        var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_tweet = parsed_data['rows'][row]['value'];
            var s_log = parsed_data['rows'][row]['key'];
            tweet.push(s_tweet);
            temp.push(s_log);
        }
        return rp(view_traffic)
		  })
		  .then(function(response){
        var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_traffic = parsed_data['rows'][row]['value'];
            traffic.push(s_traffic);
        }
        res.render('analysis2', {title: 'Alcohol, Crime, Traffic & Bottleshop', alcohol_arr: JSON.stringify(alcohol), bottleshop_arr: JSON.stringify(bottleshop), crime_arr: JSON.stringify(crime), sentiment_arr: JSON.stringify(sentiment), tweet_arr: JSON.stringify(tweet), traffic_arr: JSON.stringify(traffic), log_arr: JSON.stringify(temp)});
        return Promise.resolve();
		  })
		  .catch(function(err){console.log(err)}); // Error Handling



});

router.get('/geo/sentiment', function (req, res, next) {
    var sentiment = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/sentiment_results/_design/sentiment_map/_view/sentiment_map?reduce=true;group_level=1';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'];
                var s_sentiment = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                sentiment.push(s_sentiment);
            }
            res.render('analysis3', {title: 'Sentiment map', name_arr: JSON.stringify(name), sentiment_arr: JSON.stringify(sentiment)});
        } else {
            res.render('analysis3', {title: JSON.stringify(error)});
        }
    });
});

router.get('/geo/tweet', function (req, res, next) {
    var tweet = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/lang?reduce=true;group_level=1';
    // var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/place_name?reduce=true;group_level=2'
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'][0];
                var flag = parsed_data['rows'][row]['key'][1];
                var s_tweet = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                tweet.push(s_tweet);
     
            }
            res.render('analysis4', {title: 'Tweet map', name_arr: JSON.stringify(name), tweet_arr: JSON.stringify(tweet)});
        } else {
            res.render('analysis4', {title: JSON.stringify(error)});
        }
    });
});

router.get('/geo/politics', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/politic_map?reduce=true;group_level=1';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'];
                var s_value = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('politics', {title: 'Politics related tweet map', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('politics', {title: JSON.stringify(error)});
        }
    });
});

router.get('/geo/science', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/science_map?reduce=true;group_level=1';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'];
                var s_value = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('science', {title: 'Science related tweet map', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('science', {title: JSON.stringify(error)});
        }
    });
});

router.get('/geo/romance', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/romance_map?reduce=true;group_level=1';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'];
                var s_value = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('romance', {title: 'Romance related tweet map', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('romance', {title: JSON.stringify(error)});
        }
    });
});

router.get('/geo/language', function (req, res, next) {
    var value = [];
    var lang = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/count/_view/lang?reduce=true;group_level=2';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'][0];
                var s_lang = parsed_data['rows'][row]['key'][1];
                var s_value = parsed_data['rows'][row]['value'];
                name.push(s_name.toUpperCase());
                lang.push(s_lang);
                value.push(s_value);
            }
            res.render('languagemap', {title: 'Language map', name_arr: JSON.stringify(name), lang_arr: JSON.stringify(lang) ,value_arr: JSON.stringify(value)});
        } else {
            res.render('languagemap', {title: JSON.stringify(error)});
        }
    });
});

router.get('/scenario/language', function (req, res, next) {
    
    res.render('analysis5', {title: 'Language & Tweet'});
   
});

router.get('/scenario/employment', function (req, res, next) {
    
    res.render('analysis6', {title: 'Employment & Tweet'});
   
});

router.get('/top10/users', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.149:5984/tweet_by_usernames/_design/top10/_view/sort?descending=true';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['value'][0];
                var s_value = parsed_data['rows'][row]['value'][1];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('topuser', {title: 'Top 10 Users', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('topuser', {title: JSON.stringify(error)});
        }
    });
});
router.get('/top10/suburbs', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.149:5984/tweet_by_place/_design/top10/_view/desc?descending=true';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['value'][0];
                var s_value = parsed_data['rows'][row]['value'][1];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('topplace', {title: 'Top 10 Suburbs ', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('topplace', {title: JSON.stringify(error)});
        }
    });
});

router.get('/top10/topics', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.149:5984/tweet_by_hashtags/_design/top10/_view/sort?descending=true';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['value'][0];
                var s_value = parsed_data['rows'][row]['value'][1];
                name.push(s_name.toUpperCase());
                value.push(s_value);
            }
            res.render('toptopic', {title: 'Top 10 Topic ', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('toptopic', {title: JSON.stringify(error)});
        }
    });
});

router.get('/top10/hours', function (req, res, next) {
    var value = [];
    var name = [];
    var view_url = 'http://115.146.89.74:5984/melbourne/_design/top10/_view/hours?group_level=1';
    request(view_url, function(error, response, body){
        // Error Handling
        if(!error && response.statusCode == 200){
            //console.log(body);
            var parsed_data = JSON.parse(body);
            for(var row in parsed_data['rows']){
                var s_name = parsed_data['rows'][row]['key'];
                var s_value = parsed_data['rows'][row]['value'];
                name.push(s_name);
                value.push(s_value);
            }
            res.render('tophour', {title: 'Hour activity ', name_arr: JSON.stringify(name), value_arr: JSON.stringify(value)});
        } else {
            res.render('tophour', {title: JSON.stringify(error)});
        }
    });
});


router.get('/emoji', function (req, res, next) {
    var emoji_happy = [];
    var count_happy = [];
    var emoji_frown = [];
    var count_frown = [];
    var view_url_happy = 'http://115.146.89.74:5984/melbourne/_design/count/_view/happy?group_level=1';
    var view_url_frown = 'http://115.146.89.74:5984/melbourne/_design/count/_view/frown?group_level=1';
    rp(view_url_happy)
		  .then(function(response){
		  	var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var s_happy = parsed_data['rows'][row]['key'];
            var s_count = parsed_data['rows'][row]['value'];
            emoji_happy.push(s_happy);
            count_happy.push(s_count);
        }
		    return rp(view_url_frown)
		  })
		  .then(function(response){
        var parsed_data = JSON.parse(response);
        for(var row in parsed_data['rows']){
            var f_frown = parsed_data['rows'][row]['key'];
            var f_count = parsed_data['rows'][row]['value'];
            emoji_frown.push(f_frown);
            count_frown.push(f_count);
        }
        res.render('emoji', {title: 'Emoticon Stats', happy_arr: JSON.stringify(emoji_happy), frown_arr: JSON.stringify(emoji_frown), hcount_arr: JSON.stringify(count_happy), fcount_arr: JSON.stringify(count_frown)});
        return Promise.resolve();
		  })
		  .catch(function(err){console.log(err)}); // Error Handling
   
});

module.exports = router;
